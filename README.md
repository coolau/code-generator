 **项目介绍：** <br>
生成代码框架：改造jeesite，让生成自己的业务模块代码和界面；你也可以再次改造生成自己想要的业务代码和界面；模板代码是FreeMarker写的



 **两个项目配合使用：**<br>
1、快速生成模块代码框架，一键生成entity、model、controller、service、mapper，页面等；<br>
2、基础后台框架，功能包括CRUD、导出、导入，支持联表；自带功能权限、控制层权限、脚本攻击过滤、多数据源（AOP切面动态切换）、分表分库（mybatis拦截器动态分表和库名）；



 **生成配置界面** 
![输入图片说明](https://images.gitee.com/uploads/images/2021/0219/161548_a5d59254_5682190.png "微信图片_20210219161454.png")

 **启动项目：** <br>
1、导入基础sql，doc文件夹下面有sql文件<br>
2、修改数据库配置，config.properties文件


 **生成模块代码步骤** <br>
1、业务表配置<br>
2、生成方案配置


 **生成的代码在哪？** <br>
config.properties文件projectPath配置了本地路径<br>
![输入图片说明](https://images.gitee.com/uploads/images/2021/0219/162926_d878e4f3_5682190.png "微信图片_20210219162910.png")


 **生成的代码怎么用？** <br>
有个基础后台框架（BootStrap+Hui+Layer）：https://gitee.com/coolau/backend-base-frame



 **生成出来的界面** <br>
![输入图片说明](https://images.gitee.com/uploads/images/2020/0210/193140_cb4da37e_619481.png "微信图片_20200210193040.png")


 **基础框架的基础功能** <br>
用户管理、公司、部门、岗位、权限、登录日志、字典管理、系统操作日志、联调日志；
![输入图片说明](https://images.gitee.com/uploads/images/2021/0219/162557_593f58fb_5682190.png "微信图片_20210219162113.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0219/163819_1b5ffb90_5682190.png "微信图片_20210219163748.png")


 **运营管理下-按游戏分库界面呈现方式** <br>

![输入图片说明](https://images.gitee.com/uploads/images/2021/0612/101337_40f871da_5682190.png "微信图片_20210612101155.png")


 **联系作者：** <br>
微信：liu229053098




